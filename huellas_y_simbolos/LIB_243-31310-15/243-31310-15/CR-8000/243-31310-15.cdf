(part "243-31310-15"
    (packageRef "2433131015")
    (interface
        (port "A1" (symbPinId 1) (portName "A1") (portType INOUT))
        (port "A2" (symbPinId 2) (portName "A2") (portType INOUT))
        (port "A3" (symbPinId 3) (portName "A3") (portType INOUT))
        (port "A4" (symbPinId 4) (portName "A4") (portType INOUT))
        (port "A5" (symbPinId 5) (portName "A5") (portType INOUT))
        (port "A6" (symbPinId 6) (portName "A6") (portType INOUT))
        (port "A7" (symbPinId 7) (portName "A7") (portType INOUT))
        (port "A8" (symbPinId 8) (portName "A8") (portType INOUT))
        (port "A9" (symbPinId 9) (portName "A9") (portType INOUT))
        (port "A10" (symbPinId 10) (portName "A10") (portType INOUT))
        (port "A11" (symbPinId 11) (portName "A11") (portType INOUT))
        (port "B1" (symbPinId 12) (portName "B1") (portType INOUT))
        (port "B2" (symbPinId 13) (portName "B2") (portType INOUT))
        (port "B3" (symbPinId 14) (portName "B3") (portType INOUT))
        (port "B4" (symbPinId 15) (portName "B4") (portType INOUT))
        (port "B5" (symbPinId 16) (portName "B5") (portType INOUT))
        (port "B6" (symbPinId 17) (portName "B6") (portType INOUT))
        (port "B7" (symbPinId 18) (portName "B7") (portType INOUT))
        (port "B8" (symbPinId 19) (portName "B8") (portType INOUT))
        (port "B9" (symbPinId 20) (portName "B9") (portType INOUT))
        (port "B10" (symbPinId 21) (portName "B10") (portType INOUT))
        (port "B11" (symbPinId 22) (portName "B11") (portType INOUT))
        (port "C1" (symbPinId 23) (portName "C1") (portType INOUT))
        (port "C2" (symbPinId 24) (portName "C2") (portType INOUT))
        (port "C3" (symbPinId 25) (portName "C3") (portType INOUT))
        (port "C4" (symbPinId 26) (portName "C4") (portType INOUT))
        (port "C5" (symbPinId 27) (portName "C5") (portType INOUT))
        (port "C6" (symbPinId 28) (portName "C6") (portType INOUT))
        (port "C7" (symbPinId 29) (portName "C7") (portType INOUT))
        (port "C8" (symbPinId 30) (portName "C8") (portType INOUT))
        (port "C9" (symbPinId 31) (portName "C9") (portType INOUT))
        (port "C10" (symbPinId 32) (portName "C10") (portType INOUT))
        (port "C11" (symbPinId 33) (portName "C11") (portType INOUT))
        (port "D1" (symbPinId 34) (portName "D1") (portType INOUT))
        (port "D2" (symbPinId 35) (portName "D2") (portType INOUT))
        (port "D3" (symbPinId 36) (portName "D3") (portType INOUT))
        (port "D4" (symbPinId 37) (portName "D4") (portType INOUT))
        (port "D5" (symbPinId 38) (portName "D5") (portType INOUT))
        (port "D6" (symbPinId 39) (portName "D6") (portType INOUT))
        (port "D7" (symbPinId 40) (portName "D7") (portType INOUT))
        (port "D8" (symbPinId 41) (portName "D8") (portType INOUT))
        (port "D9" (symbPinId 42) (portName "D9") (portType INOUT))
        (port "D10" (symbPinId 43) (portName "D10") (portType INOUT))
        (port "D11" (symbPinId 44) (portName "D11") (portType INOUT))
        (port "E1" (symbPinId 45) (portName "E1") (portType INOUT))
        (port "E2" (symbPinId 46) (portName "E2") (portType INOUT))
        (port "E3" (symbPinId 47) (portName "E3") (portType INOUT))
        (port "E4" (symbPinId 48) (portName "E4") (portType INOUT))
        (port "E5" (symbPinId 49) (portName "E5") (portType INOUT))
        (port "E6" (symbPinId 50) (portName "E6") (portType INOUT))
        (port "E7" (symbPinId 51) (portName "E7") (portType INOUT))
        (port "E8" (symbPinId 52) (portName "E8") (portType INOUT))
        (port "E9" (symbPinId 53) (portName "E9") (portType INOUT))
        (port "E10" (symbPinId 54) (portName "E10") (portType INOUT))
        (port "E11" (symbPinId 55) (portName "E11") (portType INOUT))
        (port "F1" (symbPinId 56) (portName "F1") (portType INOUT))
        (port "F2" (symbPinId 57) (portName "F2") (portType INOUT))
        (port "F3" (symbPinId 58) (portName "F3") (portType INOUT))
        (port "F4" (symbPinId 59) (portName "F4") (portType INOUT))
        (port "F5" (symbPinId 60) (portName "F5") (portType INOUT))
        (port "F6" (symbPinId 61) (portName "F6") (portType INOUT))
        (port "F7" (symbPinId 62) (portName "F7") (portType INOUT))
        (port "F8" (symbPinId 63) (portName "F8") (portType INOUT))
        (port "F9" (symbPinId 64) (portName "F9") (portType INOUT))
        (port "F10" (symbPinId 65) (portName "F10") (portType INOUT))
        (port "F11" (symbPinId 66) (portName "F11") (portType INOUT))
        (port "Z1" (symbPinId 67) (portName "Z1") (portType INOUT))
        (port "Z2" (symbPinId 68) (portName "Z2") (portType INOUT))
        (port "Z3" (symbPinId 69) (portName "Z3") (portType INOUT))
        (port "Z4" (symbPinId 70) (portName "Z4") (portType INOUT))
        (port "Z5" (symbPinId 71) (portName "Z5") (portType INOUT))
        (port "Z6" (symbPinId 72) (portName "Z6") (portType INOUT))
        (port "Z7" (symbPinId 73) (portName "Z7") (portType INOUT))
        (port "Z8" (symbPinId 74) (portName "Z8") (portType INOUT))
        (port "Z9" (symbPinId 75) (portName "Z9") (portType INOUT))
        (port "Z10" (symbPinId 76) (portName "Z10") (portType INOUT))
        (port "Z11" (symbPinId 77) (portName "Z11") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "181")
    (property "Manufacturer_Name" "ept")
    (property "Manufacturer_Part_Number" "243-31310-15")
    (property "Mouser_Part_Number" "")
    (property "Mouser_Price/Stock" "")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "EPT - 243-31310-15 - MALE, PRESS FIT, TYPE C11, CL2, 77WAY")
    (property "Datasheet_Link" "https://www.datasheet5.com/search?term=243-31310-15")
    (property "symbolName1" "243-31310-15")
)
