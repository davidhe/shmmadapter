PCBNEW-LibModule-V1  2024-05-09 11:40:42
# encoding utf-8
Units mm
$INDEX
2433131015
$EndINDEX
$MODULE 2433131015
Po 0 0 0 15 663ca82a 00000000 ~~
Li 2433131015
Cd 243-31310-15-4
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 -11.250 4 1.27 1.27 0 0.254 N V 21 N "J**"
T1 -11.250 4 1.27 1.27 0 0.254 N I 21 N "2433131015"
DS -23.94 -3.7 0.94 -3.7 0.2 24
DS 0.94 -3.7 0.94 11.7 0.2 24
DS 0.94 11.7 -23.94 11.7 0.2 24
DS -23.94 11.7 -23.94 -3.7 0.2 24
DS -25 -4.7 2.5 -4.7 0.1 24
DS 2.5 -4.7 2.5 12.7 0.1 24
DS 2.5 12.7 -25 12.7 0.1 24
DS -25 12.7 -25 -4.7 0.1 24
DS -23.94 4.8 -23.94 -3.7 0.1 21
DS -23.94 -3.7 0.94 -3.7 0.1 21
DS 0.94 -3.7 0.94 11.7 0.1 21
DS 0.94 11.7 -23.94 11.7 0.1 21
DS -23.94 11.7 -23.94 7.2 0.1 21
DS 1.4 -0.2 1.4 -0.2 0.1 21
DS 1.5 -0.2 1.5 -0.2 0.1 21
DA 1.45 -0.2 1.400 -0.2 -1800 0.1 21
DA 1.45 -0.2 1.500 -0.2 -1800 0.1 21
$PAD
Po 0.000 -0
Sh "A1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 -0
Sh "A2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 -0
Sh "A3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 -0
Sh "A4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 -0
Sh "A5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 -0
Sh "A6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 -0
Sh "A7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 -0
Sh "A8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 -0
Sh "A9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 -0
Sh "A10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 -0
Sh "A11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 2
Sh "B1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 2
Sh "B2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 2
Sh "B3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 2
Sh "B4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 2
Sh "B5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 2
Sh "B6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 2
Sh "B7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 2
Sh "B8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 2
Sh "B9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 2
Sh "B10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 2
Sh "B11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 4
Sh "C1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 4
Sh "C2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 4
Sh "C3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 4
Sh "C4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 4
Sh "C5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 4
Sh "C6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 4
Sh "C7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 4
Sh "C8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 4
Sh "C9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 4
Sh "C10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 4
Sh "C11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 6
Sh "D1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 6
Sh "D2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 6
Sh "D3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 6
Sh "D4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 6
Sh "D5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 6
Sh "D6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 6
Sh "D7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 6
Sh "D8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 6
Sh "D9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 6
Sh "D10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 6
Sh "D11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 8
Sh "E1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 8
Sh "E2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 8
Sh "E3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 8
Sh "E4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 8
Sh "E5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 8
Sh "E6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 8
Sh "E7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 8
Sh "E8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 8
Sh "E9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 8
Sh "E10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 8
Sh "E11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 10
Sh "F1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 10
Sh "F2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 10
Sh "F3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 10
Sh "F4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 10
Sh "F5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 10
Sh "F6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 10
Sh "F7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 10
Sh "F8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 10
Sh "F9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 10
Sh "F10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 10
Sh "F11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 -2
Sh "Z1" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.000 -2
Sh "Z2" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.000 -2
Sh "Z3" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.000 -2
Sh "Z4" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.000 -2
Sh "Z5" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -10.000 -2
Sh "Z6" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.000 -2
Sh "Z7" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -14.000 -2
Sh "Z8" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.000 -2
Sh "Z9" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -18.000 -2
Sh "Z10" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.000 -2
Sh "Z11" C 1.100 1.100 0 0 900
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -23.000 6
Sh "MH1" C 1.000 1.000 0 0 900
Dr 2 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE 2433131015
$EndLIBRARY
