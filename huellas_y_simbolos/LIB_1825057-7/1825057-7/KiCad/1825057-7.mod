PCBNEW-LibModule-V1  2024-05-09 11:32:10
# encoding utf-8
Units mm
$INDEX
18250577
$EndINDEX
$MODULE 18250577
Po 0 0 0 15 663ca62a 00000000 ~~
Li 18250577
Cd 1825057-7-2
Kw Switch
Sc 0
At STD
AR 
Op 0 0 0
T0 3.810 8.89 1.27 1.27 0 0.254 N V 21 N "S**"
T1 3.810 8.89 1.27 1.27 0 0.254 N I 21 N "18250577"
DS 0.06 20.04 7.56 20.04 0.1 24
DS 7.56 20.04 7.56 -2.26 0.1 24
DS 7.56 -2.26 0.06 -2.26 0.1 24
DS 0.06 -2.26 0.06 20.04 0.1 24
DS -1.677 21.04 9.297 21.04 0.1 24
DS 9.297 21.04 9.297 -3.26 0.1 24
DS 9.297 -3.26 -1.677 -3.26 0.1 24
DS -1.677 -3.26 -1.677 21.04 0.1 24
DS 0.06 -1.11 0.06 -2.26 0.2 21
DS 0.06 -2.26 7.56 -2.26 0.2 21
DS 7.56 -2.26 7.56 -1.31 0.2 21
DS 0.06 18.89 0.06 20.04 0.2 21
DS 0.06 20.04 7.56 20.04 0.2 21
DS 7.56 20.04 7.56 19.09 0.2 21
DS -1.19 -0.11 -1.19 -0.11 0.1 21
DS -1.09 -0.11 -1.09 -0.11 0.1 21
DA -1.24 -0.11 -1.190 -0.11 -1800 0.1 21
DA -1.24 -0.11 -1.090 -0.11 -1800 0.1 21
$PAD
Po 0.000 -0
Sh "A1" R 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 2.54
Sh "A2" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 5.08
Sh "A3" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 7.62
Sh "A4" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 10.16
Sh "A5" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 12.7
Sh "A6" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 15.24
Sh "A7" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 17.78
Sh "A8" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 -0
Sh "B1" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 2.54
Sh "B2" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 5.08
Sh "B3" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 7.62
Sh "B4" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 10.16
Sh "B5" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 12.7
Sh "B6" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 15.24
Sh "B7" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 7.620 17.78
Sh "B8" C 1.354 1.354 0 0 900
Dr 0.79 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE 18250577
$EndLIBRARY
